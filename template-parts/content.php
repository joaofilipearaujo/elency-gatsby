<?php
/**
 * Template part for displaying content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elency-gatsby
 */

// Remove unwanted HTML comments
function sanitizeContent( $content = '' ) {
    // Remove HTML comments (blocks for example)
    $sanitizedContent = preg_replace('/<!--(.|\s)*?-->/', '', $content);

    // Remove extra line breaks
    $sanitizedContent = preg_replace("/^[\r\n]+/", "", $sanitizedContent);;
    $sanitizedContent = preg_replace("/[\r\n]+/", "\n", $sanitizedContent);;

    // Try and display elency react components as they will be parsed by gatsby
    $sanitizedContent = str_replace("[ELENCY_", "<", $sanitizedContent);
    $sanitizedContent = str_replace("[/ELENCY_", "</", $sanitizedContent);
    $sanitizedContent = str_replace("_ELENCY]", ">", $sanitizedContent);

    return $sanitizedContent;
}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
    </header><!-- .entry-header -->

	<?php elency_gatsby_post_thumbnail(); ?>

	<div class="entry-content">
		<pre><code><?php echo esc_html( sanitizeContent ( get_the_content() ) ); ?></code></pre>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
        <footer class="entry-footer">
            <?php
            edit_post_link(
                sprintf(
                    wp_kses(
                        /* translators: %s: Name of current post. Only visible to screen readers */
                        __( 'Edit <span class="screen-reader-text">%s</span>', 'elency-gatsby' ),
                        array(
                            'span' => array(
                                'class' => array(),
                            ),
                        )
                    ),
                    wp_kses_post( get_the_title() )
                ),
                '<span class="edit-link">',
                '</span>'
            );
            ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
