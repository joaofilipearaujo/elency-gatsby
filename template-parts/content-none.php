<?php
/**
 * Template part for displaying a message that content cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package elency-gatsby
 */

?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'elency-gatsby' ); ?></h1>
	</header><!-- .page-header -->
</section><!-- .no-results -->
